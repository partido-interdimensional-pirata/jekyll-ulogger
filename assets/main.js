---
---

{% node_module leaflet/dist/leaflet.js %}
{% node_module zepto/dist/zepto.min.js %}

$(document).ready(function() {
  // Mapa
  var map = L.map('map', {
    center: [ {{ site.map.center.lat }}, {{ site.map.center.lon }} ],
    zoom: {{ site.map.zoom }}
  });

  L.ColorIcon = L.DivIcon.extend({
    options: {
      html: '<svg width="5.2917mm" height="8.2181mm" version="1.1" viewBox="0 0 5.2917 8.2181" xmlns="http://www.w3.org/2000/svg"> <g transform="translate(-43.957 -96.517)"> <g transform="matrix(.66667 0 0 .66667 14.652 34.912)"> <rect x="46.42" y="94.997" width="3.0269" height="2.9833" fill="#fff"/> <path d="m47.925 104.59c0 2e-3 0.0044 7e-3 0.0044 7e-3s3.9037-5.9937 3.9037-7.9634c0-2.8988-1.9786-4.16-3.9082-4.1644-1.9296 0.0044-3.9082 1.2656-3.9082 4.1644 0 1.9697 3.906 7.9634 3.906 7.9634l0.0023-7e-3zm-1.3525-8.1216c0-0.74866 0.60606-1.3547 1.3547-1.3547 0.74866 0 1.3547 0.60606 1.3547 1.3547 0 0.74866-0.60829 1.3547-1.3569 1.3547-0.74643 0-1.3525-0.60606-1.3525-1.3547z" fill="#0f0" stroke="#000" stroke-width=".12112"/> </g> </g> </svg>',
      className: 'marker'
    },

    createIcon: function(oldIcon) {
      var icon = L.DivIcon.prototype.createIcon.call(this, oldIcon);
      // TODO esto depende mucho de la estructura del SVG!
      icon.children[0].children[0].children[0].children[1].style.fill = this.options.color;
      return icon;
    }
  });

  L.colorIcon = function(options) {
    return new L.ColorIcon(options);
  }

  // Capa de calles
  L.tileLayer('{{ site.map.tiles }}', {
    attribution: '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> / <a href="https://0xacab.org/partido-interdimensional-pirata/jekyll-ulogger">µlogger</a>',
    maxZoom: 18,
    id: 'mapbox.streets',
  }).addTo(map);

  var query = new URLSearchParams(window.location.search);
  var grupo = query.get('grupo');

  // Agregar una capa geojson vacía a la que vamos a agregar las
  // posiciones luego
  var geojson = new L.GeoJSON(undefined, {
    pointToLayer: function(feature, latlng) {
      return L.marker(latlng, { icon: L.colorIcon({ color: feature.properties.color }) });
    },
    style: function(feature) {
      return { color: feature.properties.color };
    }}).bindPopup(function(layer) {
      return layer.feature.properties.persona + ' -- ' + layer.feature.properties.time;
    })
    .addTo(map);

  $.ajax({
    type: 'GET',
    url: 'assets/comisarias.geojson',
    dataType: 'json',
    timeout: 10000,
    context: $('map'),
    success: function(data) {
      new L.GeoJSON(data, {
        pointToLayer: function(feature, latlng) {
          return L.marker(latlng, { icon: L.colorIcon({ color: 'blue' }) });
        },
        style: function(feature) {
          return { color: 'blue' };
        }}).bindPopup(function(layer) {
          return ['nombre','direccion','telefonos'].map(function(attr) {
            return layer.feature.properties[attr];
          }).join(' -- ');
        })
        .addTo(map);
    },
    error: function(xhr, type) { console.log(xhr, type) }
  });

  // Función que trae todas las posiciones
  var positions = function() {
    $.ajax({
      type: 'GET',
      url: '{{ site.map.geojson }}?grupo='+grupo,
      dataType: 'json',
      timeout: 10000,
      context: $('map'),
      success: function(data) {
        geojson.clearLayers();
        geojson.addData(data);
      },
      error: function(xhr, type) { console.log(xhr, type) }
    });
  }

  positions();

  // Actualizar automáticamente
  setInterval(positions, {{ site.map.reload }} * 1000);
});
