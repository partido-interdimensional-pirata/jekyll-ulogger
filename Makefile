.DEFAULT_GOAL := all
JEKYLL_ENV ?= production

# Directorio desde donde se sirve el sitio
DIR ?= /srv/http/ulogger.partidopirata.com.ar/
# SSH (también puede incluir opciones)
SSH ?= root@partidopirata.com.ar
# Grupo del servidor web
GRP ?= http

export

build:
	bundle exec jekyll build

release:
	rsync -rv _site/ $(SSH):$(DIR)

fix-permissions:
	ssh $(SSH) chgrp -R $(GRP) $(DIR)
	ssh $(SSH) chmod -R 750 $(DIR)

all: build release fix-permissions
